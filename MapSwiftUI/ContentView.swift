//
//  ContentView.swift
//  MapSwiftUI
//
//  Created by Администратор on 09.12.2020.
//

import SwiftUI
import MapKit

struct ContentView: View {
    @State var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 52.0, longitude: 32.5), latitudinalMeters: 10000, longitudinalMeters: 10000)
    
    var annotationArray = [
        AnnotationObject(location: CLLocationCoordinate2D(latitude: 52.0, longitude: 32.5)),
        AnnotationObject(location: CLLocationCoordinate2D(latitude: 52.05, longitude: 32.5))
    ]
    
    
    var body: some View {
        ZStack {
            Map(coordinateRegion: $region, annotationItems: annotationArray) { item in
//                MapPin(coordinate: item.location, tint: .green)
//                MapMarker(coordinate: item.location, tint: .red)
                MapAnnotation(coordinate: item.location) {
                    Image(systemName: "hare.fill").foregroundColor(.orange).font(.system(size: 32))
                }
            }
        }.edgesIgnoringSafeArea(.all)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct AnnotationObject: Identifiable {
    var id = UUID().uuidString
    var location: CLLocationCoordinate2D
}
