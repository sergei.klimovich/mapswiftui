//
//  RouteMapView.swift
//  MapSwiftUI
//
//  Created by Администратор on 11.12.2020.
//

import SwiftUI
import MapKit

struct RouteMapView: View {
    var body: some View {
        mapView()
    }
}

struct RouteMapView_Previews: PreviewProvider {
    static var previews: some View {
        RouteMapView()
    }
}


struct mapView: UIViewRepresentable {
    
    
    func makeCoordinator() -> Coordinator {
        return mapView.Coordinator()
    }
    
    func makeUIView(context: Context) -> MKMapView {
        let map = MKMapView()
        let source = CLLocationCoordinate2D(latitude: 52, longitude: 35)
        let destination = CLLocationCoordinate2D(latitude: 52.3, longitude: 35.8)
        let region = MKCoordinateRegion(center: source, latitudinalMeters: 10000, longitudinalMeters: 10000)
        
        
        map.region = region
        map.delegate = context.coordinator
        
        let detPin = MKPointAnnotation()
        detPin.coordinate = destination
        map.addAnnotation(detPin)
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: source))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination))
        
//        let directions = MKDirections(request: request)
//        
//        directions.calculate { (response, error) in
//            
//            if error != nil {
//                print(error?.localizedDescription)
//                return
//            }
//            
//            map.addOverlay(response!.routes.first!.polyline)
//            map.setRegion(MKCoordinateRegion(response!.routes.first!.polyline.boundingMapRect), animated: true)
//        }
        
        
        return map
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        
    }
    
    
    class Coordinator: NSObject, MKMapViewDelegate {
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let render = MKPolylineRenderer(overlay: overlay)
            render.strokeColor = .orange
            render.lineWidth = 2
            return render
        }
        
        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
                if let coordinate = view.annotation?.coordinate {
                    mapView.removeOverlays(mapView.overlays)
                    drawRoute(destination: coordinate, map: mapView)
                }
            }

        func drawRoute(destination: CLLocationCoordinate2D, map: MKMapView) {
             let requestDirection = MKDirections.Request()
             requestDirection.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 52, longitude: 35)))
             requestDirection.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination))
             requestDirection.transportType = .automobile
             
             let direction = MKDirections(request: requestDirection)
             
             direction.calculate { (response, error) in
                 if let errorMsg = error {
                     print(errorMsg.localizedDescription)
                 }
                 
                 if let route = response?.routes[0] {
                    
                    map.addOverlay(route.polyline)
                 }
             }
         }

    }
}
