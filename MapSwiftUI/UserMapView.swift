//
//  UserMapView.swift
//  MapSwiftUI
//
//  Created by Администратор on 09.12.2020.
//

import SwiftUI
import MapKit

struct UserMapView: View {
    var body: some View {
        UserMap()
    }
}

struct UserMap: View {
    @State var manager = CLLocationManager()
    @StateObject var locationManagerDelegate = locationManager()
    
    @State var tracking : MapUserTrackingMode = .none
    
    var body: some View {
        VStack {
            Map(coordinateRegion: $locationManagerDelegate.userRegion, interactionModes: MapInteractionModes.all, showsUserLocation: false, userTrackingMode: $tracking, annotationItems: locationManagerDelegate.userPins) { item in
                MapAnnotation(coordinate: item.location) {
                    Image(systemName: "flame.fill").foregroundColor(.red)
                }
            }
        }.edgesIgnoringSafeArea(.all)
        .onAppear(perform: {
            self.manager.delegate = locationManagerDelegate
            self.manager.startUpdatingLocation()
            
        })
        .onDisappear(perform: {
            self.manager.stopUpdatingLocation()
        })
    }
}

class locationManager: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    @Published var userPins : [AnnotationObject] = []
    @Published var userRegion : MKCoordinateRegion = MKCoordinateRegion()
    
    //Запрос разрешения у пользователя
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus != .authorizedWhenInUse {
            manager.requestWhenInUseAuthorization()
        }
    }
    //Получение его координат
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.userPins.append(AnnotationObject(location: CLLocationCoordinate2D(latitude: locations.last!.coordinate.latitude, longitude: locations.last!.coordinate.longitude)))
        self.userRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations.last!.coordinate.latitude, longitude: locations.last!.coordinate.longitude), latitudinalMeters: 10000, longitudinalMeters: 10000)
    }
    
}




















struct UserMapView_Previews: PreviewProvider {
    static var previews: some View {
        UserMapView()
    }
}
